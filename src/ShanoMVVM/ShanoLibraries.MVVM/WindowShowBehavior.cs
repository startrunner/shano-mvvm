﻿namespace ShanoLibraries.MVVM
{
    /// <summary>
    /// Used by <see cref="IDialogManager.Show(ViewModelBase, WindowShowBehavior, ViewModelBase, System.Action)"/> to define the <see cref="System.Windows.Window"/>'s behavior
    /// </summary>
    public enum WindowShowBehavior { Dialog, Window }
}
